package com.example.mvp_login.Interfaces

open class ILoginPresenter {
    open fun callLoginApi(emailId: String, password: String, providerType: String) {}
}
package com.example.mvp_login.Interfaces

import com.example.mvp_login.model.UsersList

interface ILoginView:MvpView {

    fun onSuccess(loginBase: UsersList)

    fun onError(error: Error)
}


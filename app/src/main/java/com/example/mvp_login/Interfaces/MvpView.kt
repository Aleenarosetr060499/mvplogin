package com.example.mvp_login.Interfaces
interface MvpView {
    fun showLoading()
    fun hideLoading()
    fun netWorkConnected():Boolean
    fun showMessage(message:String)
}
package com.example.mvp_login.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.mvp_login.Interfaces.ILoginView
import com.example.mvp_login.Presenter.LoginPresenter
import com.example.mvp_login.R
import com.example.mvp_login.model.UsersList
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), ILoginView {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private val loginPresenter= LoginPresenter(this,this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarbaseactivity.visibility = View.GONE

        LoginbtnLoginbutton.setOnClickListener {
            val email = EmailLogineditText.text.toString().trim()
            val password = PasswordLogineditText.text.toString().trim()
            loginPresenter.callLoginApi(email,password,"1")

        }
    }
    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }

    override fun onSuccess(loginBase: UsersList) {
        showMessage(loginBase.message!!)
        startActivity(
            Intent(this@MainActivity,HomeActivity::class.java)
        )
        hideLoading()
    }

    override fun onError(error: Error) = showMessage(error.message!!)
}

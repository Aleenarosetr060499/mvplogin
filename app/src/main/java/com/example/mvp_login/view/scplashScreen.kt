package com.example.mvp_login.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.example.mvp_login.Api.SharedPrefManager
import com.example.mvp_login.R

class scplashscreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scplash_screen)
//
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        if (SharedPrefManager.getInstance(this).isLoggedIn) {
            Handler().postDelayed({
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                //finish()
            }, 3000)
        }
        else if (!SharedPrefManager.getInstance(this).isLoggedIn){
            val  intent = Intent(applicationContext,MainActivity::class.java)
            intent.flags = android.content.Intent.FLAG_ACTIVITY_NEW_TASK or Intent
                .FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)


        }
    }
}

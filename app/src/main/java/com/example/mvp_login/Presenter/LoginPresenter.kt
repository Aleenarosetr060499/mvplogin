package com.example.mvp_login.Presenter

import android.content.Context
import com.example.mvp_login.Api.RetrofitClient
import com.example.mvp_login.Interfaces.ILoginPresenter
import com.example.mvp_login.Interfaces.ILoginView
import com.example.mvp_login.R
import com.example.mvp_login.model.UsersList
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(var iLoginView: ILoginView, var context: Context): ILoginPresenter() {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()

    override fun callLoginApi(emailId: String, password: String, providerType: String) {

        iLoginView.showLoading();
        if(iLoginView.netWorkConnected()) {
            RetrofitClient.instance.login(emailId,password,1).enqueue(object: Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) =
                    when {
                        response.code() == 400 -> {
                            val loginBase = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            iLoginView.onError(loginBase)
                        }
                        response.code() == 200 -> {
                            val loginBase = gson.fromJson(response.body().toString(), UsersList::class.java)
                            iLoginView.onSuccess(loginBase)
                        }
                        else -> {
                            iLoginView.showMessage(context.resources.getString(R.string.something_went))
                        }


                    }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                }

            })
        } else{
            iLoginView.hideLoading()
            iLoginView.showMessage(context.resources.getString(R.string.no_internet))
        }
    }
}
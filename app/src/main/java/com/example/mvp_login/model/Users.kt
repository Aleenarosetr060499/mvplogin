package com.example.mvp_login.model

data class Users(
    var user_id: Int = 0,
    var email_id: String? = null,
    var user_type: Int = 0
)
